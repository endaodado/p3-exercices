import UIKit
//la  Fonction permet de faire la somme de deux nombres
func addition (nbr1: Int, nbr2: Int) -> Int {
    let somme = nbr1 + nbr2
    return somme
}

// la fonction permet de faire la soustraction entre deux nombres
func soustraction (nbr1: Int, nbr2: Int) -> Int {
    let difference = nbr1 - nbr2
    return difference
}

// resultat de la fonction addition en premier parametre
 soustraction(nbr1: addition(nbr1: 18, nbr2: 19), nbr2: 8)

// Fonction qui affiche une liste des nombres impaires
func nombreImpaires (listeDesChiffres : [Int]) -> [Int]{
    var listImpaire : [Int] = []
    for i in listeDesChiffres {
        if (i % 2) != 0 {
            listImpaire.append(i)
        }
    }
    return listImpaire
}
 nombreImpaires(listeDesChiffres: [1 , 2 , 3 , 4 , 5 , 6 , 7 , 8])

// theoreme de Pythagore
/*
        C   *
            ***
            ****
            *****
            ******
            *******
            ********
            *********
            **********
         A  *********** B
*/

func pythagore (cote1 AB : Float , cote2 AC : Float) -> Float{
    let hypotenuse = (pow(AB, 2) + pow(AC, 2)).squareRoot()
    return hypotenuse
    
}
pythagore(cote1: 2, cote2: 4)



