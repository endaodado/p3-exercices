import UIKit

class Character {
    var lifePoint : Int
    var attackPoint : Int
    var name : String
    
   
    
    init(lifePoint : Int, attackPoint : Int , name : String) {
        
        self.lifePoint = lifePoint
        self.attackPoint = attackPoint
        self.name = name
    }
    
    // character presentation function
    func present() {
        print("life point : \(lifePoint)"
        + "\n attack point : \(attackPoint) ")
    }
    
     // fonction qui definie le personnage le plus fort
    func strongest(charter : Character){
        if charter.lifePoint > lifePoint && charter.attackPoint > attackPoint{
            print(" \(charter.name) is stronger than \(name) ")
        }else {
             print(" \(name) is stronger than \(charter.name) ")
        }
    }
    
    // Fonction d'action
    func actionOn (charter : Character){
        charter.lifePoint -= attackPoint
    }
}
// Classe Warrior heritant de la classe Character
class Warrior : Character {
    var characterType : String
    init(lifePoint: Int, attackPoint: Int, name: String, characterType :String) {
        self.characterType = characterType
        super.init(lifePoint: lifePoint, attackPoint: attackPoint, name: name)
    }
    override func present() {
        print("Character : \(characterType)"
            + "\n life point : \(lifePoint)"
            + "\n attack point : \(attackPoint) ")
    }
}


// Classe Magus heritant de la classe Character
class Magus : Character {
    var characterType : String
    init(lifePoint: Int, attackPoint: Int , name : String , characterType : String) {
        self.characterType = characterType
        super.init(lifePoint: lifePoint, attackPoint: attackPoint, name: name)
    }
    override func present() {
        print("Character : \(characterType)"
            + "\n life point : \(lifePoint)"
            + "\n attack point : \(attackPoint) ")
    }
}

// Classe Priest heritant de la classe Character
class Priest : Character {
    override func actionOn(charter: Character) {
    
             charter.lifePoint += attackPoint
    }
}


